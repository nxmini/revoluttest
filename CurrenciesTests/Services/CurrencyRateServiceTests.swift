//
//  CurrencyRateServiceTests.swift
//  CurrenciesTests
//
//  Created by Павел В. Баранов on 18/02/2019.
//

import XCTest
@testable import Currencies
import Moya

class CurrencyRateServiceTests: XCTestCase {

    typealias APIProvider = MoyaProvider<RevolutAPI>

    func testServiceShouldReturnRatesOnValidResponse() throws {
        let fixtureName = "CurrencyRateServiceTests_ValidRatesResponse"
        let stubbingProvider = try APIProvider.makeStub(fixtureName: fixtureName)
        let service = CurrencyRateServiceImpl(provider: stubbingProvider)

        validateResultOfRatesRequest(service: service) {
            guard case .success = $0 else {
                XCTFail("result should be success")
                return
            }
        }
    }

    func testSeriviceShouldReturnErrorOnNetworkFailure() {
        let stubbingProvider = APIProvider.makeStubWithNetrowrkError()
        let service = CurrencyRateServiceImpl(provider: stubbingProvider)

        validateResultOfRatesRequest(service: service) {
            guard case .failure = $0 else {
                XCTFail("result should be failure")
                return
            }
        }
    }

    func testServiceShouldReturnErrorOnUnsuccessfulStatusCode() {
        let stubbingProvider = APIProvider.makeStub(responseCode: 401, data: Data())
        let service = CurrencyRateServiceImpl(provider: stubbingProvider)

        validateResultOfRatesRequest(service: service) {
            guard case .failure = $0 else {
                XCTFail("result should be failure")
                return
            }
        }
    }

    func testServiceShouldReturnErrorOnEmptyRepsponse() {
        let stubbingProvider = APIProvider.makeStub(responseCode: 200, data: Data())
        let service = CurrencyRateServiceImpl(provider: stubbingProvider)

        validateResultOfRatesRequest(service: service) {
            guard case .failure = $0 else {
                XCTFail("result should be failure")
                return
            }
        }
    }

}

private extension CurrencyRateServiceTests {
    func validateResultOfRatesRequest(service: CurrencyRateService,
                                      base: String = "USD",
                                      validation: @escaping AppCompletion<CurrencyRates>) {
        let exp = expectation(description: "")
        service.getCurrencyRates(base: base) {
            validation($0)
            exp.fulfill()
        }
        wait(for: [exp], timeout: 5)
    }
}
