//
//  CurrencyRateServiceMock.swift
//  CurrenciesTests
//
//  Created by Павел В. Баранов on 21/02/2019.
//

import Foundation
import Moya
import Result
@testable import Currencies

class CancellableStub: Cancellable {
    var isCancelled: Bool = false

    func cancel() {
        isCancelled = true
    }
}

class CurrencyRateServiceMock: CurrencyRateService {

    enum MockError: Error {
        case notConfigured
    }

    var nextResult: AppResult<CurrencyRates> = .failure(AnyError(MockError.notConfigured))

    func getCurrencyRates(base: String, completion: @escaping AppCompletion<CurrencyRates>) -> Cancellable {

        completion(nextResult)
        return CancellableStub()
    }
}
