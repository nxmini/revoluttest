//
//  ManualActionTrigger.swift
//  CurrenciesTests
//
//  Created by Павел В. Баранов on 21/02/2019.
//

import Foundation
@testable import Currencies

class ManualActionTrigger: RepeatingActionTrigger {
    var action: (() -> Void)?

    func setAction(_ action: @escaping () -> Void) {
        self.action = action
    }

    func start() {
        action?()
    }

    func stop() {}
}
