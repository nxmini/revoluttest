//
//  CurrenciesViewModelTests.swift
//  CurrenciesTests
//
//  Created by Павел В. Баранов on 21/02/2019.
//

import XCTest
import Result
@testable import Currencies

class CurrenciesViewModelTests: XCTestCase {

    enum TestLogicError: Error {
        case unexceptedValue
    }

    var viewModel: CurrenciesViewModel! = nil
    var trigger: RepeatingActionTrigger! = nil
    var serviceMock: CurrencyRateServiceMock! = nil

    override func setUp() {
        let trigger = ManualActionTrigger()
        let service = CurrencyRateServiceMock()
        let viewModel = CurrenciesViewModel(currencyRateService: service, updateTrigger: trigger)

        self.viewModel = viewModel
        self.serviceMock = service
        self.trigger = trigger
    }

    override func tearDown() {
        self.viewModel = nil
        self.trigger = nil
        self.serviceMock = nil
    }

    func testModelShouldTransitionToLoadingStateAfterFirstStart() {
        let rates = CurrencyRates.init(base: "USD", rates: [:])
        let exp = expectation(description: "")

        viewModel.onStateChange = {
            if $0 == .loading {
                exp.fulfill()
            }
        }

        makeSuccessfullInitialLoad(data: rates)
        wait(for: [exp], timeout: 2)
    }

    func testModelShouldTransitionToLoadedStateAfterFirstSuccessfullLoad() {
        let rates = CurrencyRates.init(base: "USD", rates: [:])
        let exp = expectation(description: "")
        viewModel.onStateChange = {
            if $0 == .loaded {
                exp.fulfill()
            }
        }
        makeSuccessfullInitialLoad(data: rates)
        wait(for: [exp], timeout: 2)
    }

    func testModelShouldTransitionToErrorStateAfterUnsuccessfullFirstLoad() {
        let exp = expectation(description: "")
        viewModel.onStateChange = {
            if $0 == .error {
                exp.fulfill()
            }
        }
        viewModel.start()
        wait(for: [exp], timeout: 2)
    }

    func testModelShouldTransitionToUpdatingStateOnSubsequentRateUpdates() {
        let rates = CurrencyRates(base: "USD", rates: [:])
        makeSuccessfullInitialLoad(data: rates)

        let exp = expectation(description: "")
        viewModel.onStateChange = {
            if $0 == .updating {
                exp.fulfill()
            }
        }
        trigger.start()
        wait(for: [exp], timeout: 2)
    }

    func testModelShouldTransitionToUpdatedStateAfterRatesUpdate() {
        let rates = CurrencyRates(base: "USD", rates: [:])
        makeSuccessfullInitialLoad(data: rates)

        let exp = expectation(description: "")
        viewModel.onStateChange = {
            if $0 == .updated {
                exp.fulfill()
            }
        }
        trigger.start()
        wait(for: [exp], timeout: 2)
    }

    func testModelShouldTransitionToOfflineStateAfterServiceFailureIfRatesLoaded() {
        let rates = CurrencyRates(base: "USD", rates: [:])
        makeSuccessfullInitialLoad(data: rates)

        let exp = expectation(description: "")
        viewModel.onStateChange = {
            if $0 == .offline {
                exp.fulfill()
            }
        }
        serviceMock.nextResult = .failure(AnyError(CurrencyRateServiceMock.MockError.notConfigured))
        trigger.start()
        wait(for: [exp], timeout: 2)
    }

    func testBaseCurrencyShouldBeAtFirstPositionOfValueModelsList() throws {
        let base = "EUR"
        let rates = try makeMockRates(base: base)
        makeSuccessfullInitialLoad(data: rates)
        let viewModels = viewModel.currencyAmountViewModels
        XCTAssertFalse(viewModels.isEmpty)
        XCTAssertEqual(viewModels[0].title, base)
    }

    func testCurrencyPositionShouldChangeToFirstAfterChangingBaseCurrency() throws {
        let base = "EUR"
        let rates = try makeMockRates(base: base)
        makeSuccessfullInitialLoad(data: rates)
        let newBase = "RUB"
        viewModel.changeBaseCurrency(to: newBase)
        let viewModels = viewModel.currencyAmountViewModels
        XCTAssertFalse(viewModels.isEmpty)
        XCTAssertEqual(viewModels[0].title, newBase)
    }

    func testCurrencyRateValuesShouldBeRecalculatedOnBaseAmountChange() throws {
        let base = "EUR"
        let rates = try makeMockRates(base: base)
        makeSuccessfullInitialLoad(data: rates)

        let amount = 1.234
        viewModel.changeBaseAmount(amount)

        let valueModels = viewModel.currencyAmountViewModels.dropFirst()
        try valueModels.forEach {
            guard let currencyRate = rates.rates[$0.title] else { throw TestLogicError.unexceptedValue }
            XCTAssertEqual($0.amount, currencyRate * amount)
        }
    }

    func testCurrencyValuesShouldBeRecalculatedAfterBaseChange() throws {
        let base = "EUR"
        let rates = try makeMockRates(base: base)

        makeSuccessfullInitialLoad(data: rates)

        let newBase = "RUB"
        viewModel.changeBaseCurrency(to: newBase)
        viewModel.changeBaseAmount(1)

        let valueModels = viewModel.currencyAmountViewModels.dropFirst()
        try valueModels.forEach {
            guard let newBaseRate = rates.rates[newBase] else { throw TestLogicError.unexceptedValue }
            let coefficient = 1 / newBaseRate
            if $0.title == base {
                XCTAssertEqual($0.amount, coefficient)
            } else {
                guard let rate = rates.rates[$0.title] else { throw TestLogicError.unexceptedValue }
                XCTAssertEqual($0.amount, rate * coefficient)
            }
        }
    }
}

private extension CurrenciesViewModelTests {
    func makeSuccessfullInitialLoad(data: CurrencyRates) {
        serviceMock.nextResult = .success(data)

        let originalStateChange = viewModel.onStateChange

        let exp = expectation(description: "")
        viewModel.onStateChange = {
            originalStateChange?($0)
            if $0 == .loaded {
                exp.fulfill()
            }
        }
        viewModel.start()
        wait(for: [exp], timeout: 2)
    }

    func mockRateCoefficients() -> [String: Double] {
        return [
            "EUR": 1,
            "USD": 2,
            "RUB": 3,
            "CAD": 4
        ]
    }

    func makeMockRates(base: String) throws -> CurrencyRates {
        //swiftlint:disable nesting
        enum MockError: Error {
            case unsupportedMockCurrency
        }
        //swiftlint:enable nesting

        var coefficients = mockRateCoefficients()
        guard let baseCoeficient = coefficients[base] else { throw MockError.unsupportedMockCurrency }

        coefficients[base] = nil

        var rates: [String: Double] = [:]
        coefficients.forEach { rates[$0.key] = $0.value / baseCoeficient }
        return CurrencyRates(base: base, rates: rates)
    }
}
