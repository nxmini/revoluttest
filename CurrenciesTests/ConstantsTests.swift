//
//  ConstantsTests.swift
//  CurrenciesTests
//
//  Created by Павел В. Баранов on 18/02/2019.
//

import XCTest
@testable import Currencies

class CurrenciesTests: XCTestCase {

    func testBaseUrlIsValid() {
        let url = URL(string: RevolutAPI.Constants.baseUrl)
        XCTAssertNotNil(url)
    }

    func testCurrencyRatesRequestPathIsValid() {
        XCTAssertEqual(RevolutAPI.Constants.Paths.currencyRates, "latest")
    }

    func testBaseCurrencyParaneterIsValid() {
        XCTAssertEqual(RevolutAPI.Constants.Parameters.baseCurrency, "base")
    }

}
