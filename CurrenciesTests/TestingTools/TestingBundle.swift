//
//  TestingBundle.swift
//  CurrenciesTests
//
//  Created by Павел В. Баранов on 19/02/2019.
//

import Foundation

class TestingBundle: Bundle {
    class func defaulTextingBundle() -> Bundle {
        return Bundle(for: self)
    }
}
