//
//  XCTestCase+Tools.swift
//  CurrenciesTests
//
//  Created by Павел В. Баранов on 19/02/2019.
//

import XCTest

extension XCTestCase {

    enum Constants {
        static let defaultStubDelay = 0.2
    }

    enum FixtureError: Error {
        case fixtureNotFound
    }

    static func loadData(fixtureName: String, fileExtension: String = "json") throws -> Data {
        let bundle = TestingBundle.defaulTextingBundle()
        guard let fileUrl = bundle.url(forResource: fixtureName, withExtension: fileExtension) else {
            throw FixtureError.fixtureNotFound
        }
        let data = try Data(contentsOf: fileUrl)
        return data
    }
}
