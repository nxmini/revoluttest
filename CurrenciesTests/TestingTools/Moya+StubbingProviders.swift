//
//  Moya+StubbingProviders.swift
//  CurrenciesTests
//
//  Created by Павел В. Баранов on 19/02/2019.
//

import XCTest
import Moya

extension MoyaProvider where Target: TargetType {

    class func makeStubWithNetrowrkError() -> MoyaProvider<Target> {
        let networkError = NSError(domain: NSURLErrorDomain ,
                                   code: NSURLErrorNotConnectedToInternet,
                                   userInfo: nil)

        let customEndpointClosure = { (target: Target) -> Endpoint in
            return Endpoint(url: URL(target: target).absoluteString,
                            sampleResponseClosure: { .networkError(networkError) },
                            method: target.method,
                            task: target.task,
                            httpHeaderFields: target.headers)
        }

        let delay = XCTestCase.Constants.defaultStubDelay
        let stubbingProvider = MoyaProvider<Target>(endpointClosure: customEndpointClosure,
                                               stubClosure: MoyaProvider.delayedStub(delay))
        return stubbingProvider
    }

    class func makeStub(responseCode: Int = 200, data: Data) -> MoyaProvider<Target> {
        let customEndpointClosure = { (target: Target) -> Endpoint in
            return Endpoint(url: URL(target: target).absoluteString,
                            sampleResponseClosure: { .networkResponse(responseCode, data) },
                            method: target.method,
                            task: target.task,
                            httpHeaderFields: target.headers)
        }

        let delay = XCTestCase.Constants.defaultStubDelay
        let stubbingProvider = MoyaProvider<Target>(endpointClosure: customEndpointClosure,
                                                        stubClosure: MoyaProvider.delayedStub(delay))
        return stubbingProvider
    }

    class func makeStub(responseCode: Int = 200, fixtureName: String) throws -> MoyaProvider<Target> {
        let data = try XCTestCase.loadData(fixtureName: fixtureName)
        return makeStub(responseCode: responseCode, data: data)
    }
}
