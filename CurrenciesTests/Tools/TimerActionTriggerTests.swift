//
//  TimerActionTriggerTests.swift
//  CurrenciesTests
//
//  Created by Павел В. Баранов on 20/02/2019.
//

import XCTest
@testable import Currencies

class TimerActionTriggerTests: XCTestCase {

    func testTimerTriggerShouldCallActionAfterStart() {
        let trigger = TimerActionTrigger(timeInterval: 1)

        let exp = expectation(description: "")
        trigger.setAction { exp.fulfill() }
        trigger.start()

        wait(for: [exp], timeout: 2)
    }

    func testTimerTriggerShouldNotCallActionIfIsNotStarted() {
        let trigger = TimerActionTrigger(timeInterval: 1)

        let exp = expectation(description: "")

        var called = false
        trigger.setAction {
            called = true
        }
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2) {
            exp.fulfill()
        }
        wait(for: [exp], timeout: 5)

        XCTAssertFalse(called)
    }

    func testTimerTriggerShouldFireRepeatedlyAfterStart() {
        let times = 5
        var expectations: [XCTestExpectation] = []
        for idx in 0..<times {
            let exp = expectation(description: "\(idx)")
            expectations.append(exp)
        }

        let timeInterval = 1
        let trigger = TimerActionTrigger(timeInterval: 1)
        var index = 0
        trigger.setAction {
            if index >= times {
                XCTFail("Test logic failure. Index out of bounds")
            }
            expectations[index].fulfill()
            index += 1
        }

        trigger.start()

        wait(for: expectations, timeout: TimeInterval((timeInterval * times) + 1))
    }

    func testTimerTriggerShouldCallActionEveryGivenTime() {
        let time: TimeInterval = 1
        let trigger = TimerActionTrigger(timeInterval: time)
        var currentTime = Date()

        var deltas: [TimeInterval] = []
        trigger.setAction {
            let actionDate = Date()
            let diff = Date().timeIntervalSince(currentTime)
            deltas.append(diff)
            currentTime = actionDate
        }

        let exp = expectation(description: "")
        let testPeriod = time * 5

        trigger.start()
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + testPeriod) {
            exp.fulfill()
        }
        wait(for: [exp], timeout: testPeriod + 1)

        print(deltas)

        deltas.forEach {
            XCTAssertTrue( (time - 0.01) < $0 )
            XCTAssertTrue( (time + 0.01) > $0 )
        }
    }

    func testTimerTriggerShouldNotCallActionAfterStop() {
        let time: TimeInterval = 1
        let trigger = TimerActionTrigger(timeInterval: time)

        let actionCallExpectation = expectation(description: "action called")
        trigger.setAction { [weak trigger] in
            trigger?.stop()
            actionCallExpectation.fulfill()
        }
        trigger.start()

        wait(for: [actionCallExpectation], timeout: 2)

        var called = false
        trigger.setAction {
            called = true
        }

        let exp = expectation(description: "")
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2) {
            exp.fulfill()
        }
        wait(for: [exp], timeout: 5)

        XCTAssertFalse(called)
    }

    func testTimerTriggerShouldNotCallActionAfterTriggerInstanceRemoval() {
        var trigger: TimerActionTrigger? = TimerActionTrigger(timeInterval: 2)

        var called = false
        trigger!.setAction {
            called = true
        }
        trigger!.start()
        trigger = nil

        let exp = expectation(description: "")
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 4) {
            exp.fulfill()
        }

        wait(for: [exp], timeout: 5)

        XCTAssertFalse(called)
    }
}
