//
//  RevolutApiTests.swift
//  CurrenciesTests
//
//  Created by Павел В. Баранов on 18/02/2019.
//

import XCTest
@testable import Currencies
import Moya

class RevolutApiTests: XCTestCase {

    func testBaseURL() {
        XCTAssertEqual(RevolutAPI.rates(base: "123").baseURL,
                       URL(string: RevolutAPI.Constants.baseUrl))
    }

    func testCurrencyRatesPath() {
        XCTAssertEqual(RevolutAPI.rates(base: "123").path,
                       RevolutAPI.Constants.Paths.currencyRates)
    }

    func testCurrencyRatesHttpMethod() {
        XCTAssertEqual(RevolutAPI.rates(base: "123").method,
                       .get)
    }

    func testCurrencyRatesParametersEncoding() {
        let base = "mock base"
        let task = RevolutAPI.rates(base: base).task
        guard case .requestParameters(_, let encoding) = task else {
            XCTFail("Currency rates request paramters should be encoded as http-get parameters")
            return
        }
        XCTAssertTrue(encoding is URLEncoding)
    }

    func testCurrencyRatesParameters() {
        let base = "mock base"
        let task = RevolutAPI.rates(base: base).task
        guard case .requestParameters(let parameters, _) = task else {
            XCTFail("Currency rates request paramters should be encoded as http-get parameters")
            return
        }

        XCTAssertEqual(parameters[RevolutAPI.Constants.Parameters.baseCurrency] as? String,
                       base)
    }

    func testCurrencyRatesContentTypeHeader() {
        let base = "mock base"
        let headers = RevolutAPI.rates(base: base).headers

        XCTAssertEqual(headers?["Content-type"], "application/json")
    }
}
