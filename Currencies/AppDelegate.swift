//
//  AppDelegate.swift
//  Currencies
//
//  Created by Павел В. Баранов on 09/02/2019.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let service: CurrencyRateService = CurrencyRateServiceAssembly.makeService()

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?)
        -> Bool {

        if ProcessInfo.processInfo.environment["XCInjectBundleInto"] != nil {
            return false
        }

        let window = UIWindow(frame: UIScreen.main.bounds)
        let rootViewController = CurrenciesModuleAssembly.makeModule()
        let navigationController = UINavigationController(rootViewController: rootViewController)
        window.rootViewController = navigationController
        self.window = window
        window.makeKeyAndVisible()
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {}

    func applicationDidEnterBackground(_ application: UIApplication) {}

    func applicationWillEnterForeground(_ application: UIApplication) {}

    func applicationDidBecomeActive(_ application: UIApplication) {}

    func applicationWillTerminate(_ application: UIApplication) {}
}
