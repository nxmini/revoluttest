//
//  CurrencyRateServiceImpl.swift
//  Currencies
//
//  Created by Павел В. Баранов on 09/02/2019.
//

import Foundation
import Moya
import Result

struct CurrencyRateServiceImpl: CurrencyRateService {

    private let provider: MoyaProvider<RevolutAPI>

    init(provider: MoyaProvider<RevolutAPI>) {
        self.provider = provider
    }

    @discardableResult
    func getCurrencyRates(base: String, completion: @escaping AppCompletion<CurrencyRates>) -> Cancellable {
        return provider.request(.rates(base: base)) {
            completion (
                $0.tryMap { try $0.filterSuccessfulStatusCodes() }
                  .tryMap { try $0.map(CurrencyRates.self) }
                  .mapError { AnyError($0) }
            )
        }
    }
}
