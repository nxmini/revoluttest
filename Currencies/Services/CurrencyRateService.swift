//
//  CurrencyRateService.swift
//  Currencies
//
//  Created by Павел В. Баранов on 09/02/2019.
//

import Result
import Moya

protocol CurrencyRateService {
    @discardableResult
    func getCurrencyRates(base: String, completion: @escaping AppCompletion<CurrencyRates>) -> Cancellable
}
