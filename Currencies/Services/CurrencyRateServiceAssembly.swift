//
//  CurrencyRateServiceAssembly.swift
//  Currencies
//
//  Created by Павел В. Баранов on 09/02/2019.
//

import Moya

enum CurrencyRateServiceAssembly {

    static func makeService() -> CurrencyRateService {
        let provider = MoyaProvider<RevolutAPI>()
        return CurrencyRateServiceImpl(provider: provider)
    }

}
