//
//  CurrenciesViewModel.swift
//  Currencies
//
//  Created by Павел В. Баранов on 10/02/2019.
//

import Foundation
import DifferenceKit
import Moya
import FlagKit

class CurrenciesViewModel {

    struct CurrencyAmountViewModel {
        let title: String
        let amount: Double
        let image: UIImage?
        let currencyName: String?
    }

    private let currencyRateService: CurrencyRateService
    private let trigger: RepeatingActionTrigger

    private var baseCurrency = "EUR"
    private var baseAmount: Double = 100
    private var lastRates: CurrencyRates?
    private var updateTask: Cancellable?

    init(currencyRateService: CurrencyRateService,
         updateTrigger: RepeatingActionTrigger) {
        self.currencyRateService = currencyRateService
        self.trigger = updateTrigger
        currencyAmountViewModels = []
    }

    var onStateChange: ((State) -> Void)?
    var onBaseAmountChange: (() -> Void)?

    private(set) var currencyAmountViewModels: [CurrencyAmountViewModel]

    enum State: Equatable {
        case initial
        case loading
        case loaded
        case updating
        case updated
        case offline
        case error
    }

    private(set) var state: State = .initial {
        didSet { onStateChange?(state) }
    }

    func start() {

        trigger.setAction { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.updateRates()
        }

        trigger.start()
    }

    func stop() {
        trigger.stop()
    }

    private func updateRates() {
        state = (lastRates == nil) ? (.loading) : (.updating)
        updateTask = currencyRateService.getCurrencyRates(base: baseCurrency) { [weak self] result in
            DispatchQueue.main.async {
                guard let strongSelf = self else { return }
                switch result {
                case .success(let rates):
                    strongSelf.lastRates = rates
                    strongSelf.changeAmounts(rates: rates)
                    strongSelf.state = (strongSelf.state == .loading) ? .loaded : .updated
                case .failure:
                    if strongSelf.state == .loading {
                        strongSelf.state = .error
                        strongSelf.stop()
                    } else {
                        strongSelf.state = .offline
                    }
                }
            }
        }
    }

    func changeBaseCurrency(to newBase: String, withAmount newAmount: Double = 100) {
        guard let lastRates = lastRates else { return }

        updateTask?.cancel()
        trigger.stop()

        let rates = recalculateRates(rates: lastRates, withNewBase: newBase)
        baseCurrency = newBase
        baseAmount = newAmount

        self.lastRates = rates

        changeAmounts(rates: rates)

        trigger.start()
    }

    func changeBaseAmount(_ amount: Double) {
        baseAmount = amount
        guard let lastRates = lastRates else { return }
        changeAmounts(rates: lastRates)
        onBaseAmountChange?()
    }

    private func changeAmounts(rates: CurrencyRates) {
        var amountModels: [CurrencyAmountViewModel] = []

        let baseAmountModel = makeCurrencyViewModel(currency: baseCurrency,
                                                    amount: baseAmount)
        amountModels.append(baseAmountModel)

        let otherModels = rates.rates
                          .map { makeCurrencyViewModel(currency: $0, amount: $1 * baseAmount) }
                          .sorted { $0.title < $1.title }
        amountModels.append(contentsOf: otherModels)

        currencyAmountViewModels = amountModels
    }

    private func recalculateRates(rates: CurrencyRates, withNewBase newBase: String) -> CurrencyRates {

        var ratesDictionary = rates.rates

        guard let newBaseRate = ratesDictionary.removeValue(forKey: newBase) else { return rates }
        let coeficient = 1 / newBaseRate

        var newRatesDictionary = ratesDictionary.mapValues { $0 * coeficient }
        newRatesDictionary[rates.base] = coeficient

        let rates = CurrencyRates(base: newBase, rates: newRatesDictionary)
        return rates
    }

    private func makeCurrencyViewModel(currency: String, amount: Double) -> CurrencyAmountViewModel {
        let title = currency
        let amount = amount
        let image = FlagKit.flagImage(currencyCode: currency, style: .circle)
        let currencyName = Locale.current.localizedString(forCurrencyCode: currency)

        return CurrencyAmountViewModel(title: title,
                                       amount: amount,
                                       image: image,
                                       currencyName: currencyName)
    }
}

extension CurrenciesViewModel.CurrencyAmountViewModel: Differentiable {

    var differenceIdentifier: Int {
        return title.hashValue
    }

    func isContentEqual(to source: CurrenciesViewModel.CurrencyAmountViewModel) -> Bool {
        return amount == source.amount
    }

}
