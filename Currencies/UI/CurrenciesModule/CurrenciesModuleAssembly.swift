//
//  CurrenciesModuleAssembly.swift
//  Currencies
//
//  Created by Павел В. Баранов on 10/02/2019.
//

import UIKit

enum CurrenciesModuleAssembly {
    static func makeModule() -> UIViewController {
        let service = CurrencyRateServiceAssembly.makeService()
        let trigger = TimerActionTrigger(timeInterval: 1)
        let viewModel = CurrenciesViewModel(currencyRateService: service, updateTrigger: trigger)
        let view = CurrenciesViewController(viewModel: viewModel)
        return view
    }
}
