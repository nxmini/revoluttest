//
//  CurrencyValueCell.swift
//  Currencies
//
//  Created by Павел В. Баранов on 18/02/2019.
//

import UIKit

class CurrencyValueCell: UITableViewCell {

    private(set) lazy var currencyTitleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        UIFont.preferredFont(forTextStyle: UIFont.TextStyle.title1)
        return label
    }()

    private(set) lazy var currencyNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.footnote)
        label.textColor = UIColor.lightGray
        return label
    }()

    private lazy var currencyTitleView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    private(set) lazy var amountTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.keyboardType = .decimalPad
        textField.textAlignment = .right
        textField.isUserInteractionEnabled = false
        textField.font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.body)
        return textField
    }()

    private(set) lazy var flagImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .value1, reuseIdentifier: reuseIdentifier)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setup() {

        currencyTitleView.addSubview(currencyTitleLabel)
        currencyTitleView.addSubview(currencyNameLabel)
        contentView.addSubview(currencyTitleView)

        contentView.addSubview(flagImageView)
        contentView.addSubview(amountTextField)

        NSLayoutConstraint.activate([
            flagImageView.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 20),
            flagImageView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            flagImageView.topAnchor.constraint(greaterThanOrEqualTo: contentView.topAnchor),
            flagImageView.widthAnchor.constraint(equalToConstant: 50),
            flagImageView.heightAnchor.constraint(equalToConstant: 50),
            flagImageView.bottomAnchor.constraint(lessThanOrEqualTo: contentView.bottomAnchor),

            currencyTitleLabel.leftAnchor.constraint(equalTo: currencyTitleView.leftAnchor),
            currencyTitleLabel.topAnchor.constraint(equalTo: currencyTitleView.topAnchor),
            currencyTitleLabel.rightAnchor.constraint(equalTo: currencyTitleView.rightAnchor),

            currencyNameLabel.leftAnchor.constraint(equalTo: currencyTitleView.leftAnchor),
            currencyNameLabel.topAnchor.constraint(equalTo: currencyTitleLabel.bottomAnchor),
            currencyNameLabel.rightAnchor.constraint(equalTo: currencyTitleView.rightAnchor),
            currencyNameLabel.bottomAnchor.constraint(equalTo: currencyTitleView.bottomAnchor),

            currencyTitleView.topAnchor.constraint(greaterThanOrEqualTo: contentView.topAnchor),
            currencyTitleView.leftAnchor.constraint(equalTo: flagImageView.rightAnchor,
                                                     constant: 20),
            currencyTitleView.rightAnchor.constraint(lessThanOrEqualTo: contentView.rightAnchor,
                                                      constant: -20),
            currencyTitleView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            currencyTitleView.bottomAnchor.constraint(lessThanOrEqualTo: contentView.bottomAnchor),

            amountTextField.topAnchor.constraint(greaterThanOrEqualTo: contentView.topAnchor),
            amountTextField.leftAnchor.constraint(greaterThanOrEqualTo: currencyTitleLabel.rightAnchor,
                                                  constant: 10),
            amountTextField.rightAnchor.constraint(lessThanOrEqualTo: contentView.rightAnchor,
                                                   constant: -20),
            amountTextField.centerYAnchor.constraint(equalTo: contentView.centerYAnchor)
            ])
    }
}
