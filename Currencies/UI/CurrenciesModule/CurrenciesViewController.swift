//
//  CurrenciesViewController.swift
//  Currencies
//
//  Created by Павел В. Баранов on 10/02/2019.
//

import UIKit
import Typist
import DifferenceKit
import FlagKit

class CurrenciesViewController: UIViewController {

    private let viewModel: CurrenciesViewModel
    private let keyboard = Typist()
    private let numberFormatter: NumberFormatter

    private var tableViewDataSource: [CurrenciesViewModel.CurrencyAmountViewModel] = []

    init(viewModel: CurrenciesViewModel) {
        self.viewModel = viewModel

        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.maximumFractionDigits = 3
        self.numberFormatter = formatter

        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    lazy var activityIndicator: UIActivityIndicatorView = {
        let view = UIActivityIndicatorView(style: .gray)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    lazy var tableView: UITableView = {
        let view = UITableView()
        view.delegate = self
        view.dataSource = self
        view.translatesAutoresizingMaskIntoConstraints = false
        view.keyboardDismissMode = .interactive
        view.isHidden = true
        return view
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        keyboard.start()
    }

    override func viewWillDisappear(_ animated: Bool) {
        keyboard.stop()
        super.viewWillDisappear(animated)
    }
}

private extension CurrenciesViewController {
    func setupView() {
        view.backgroundColor = .white
        tableView.registerCell(CurrencyValueCell.self)
        setupLayout()
        setupKeyboardAvoiding()
        setupViewModel()
    }

    func setupLayout() {
        view.addSubview(tableView)
        view.addSubview(activityIndicator)

        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.topAnchor),
            tableView.leftAnchor.constraint(equalTo: view.leftAnchor),
            tableView.rightAnchor.constraint(equalTo: view.rightAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),

            activityIndicator.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            activityIndicator.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        ])
    }

    func setupKeyboardAvoiding() {
        let scrollView = tableView
        keyboard
            .on(event: .willShow) {
                let currentInset = scrollView.contentInset
                let newContentInset = UIEdgeInsets(top: currentInset.top,
                                                    left: currentInset.left,
                                                    bottom: $0.endFrame.size.height,
                                                    right: currentInset.right)
                scrollView.contentInset = newContentInset
                scrollView.scrollIndicatorInsets = newContentInset
            }
            .on(event: .didHide) { _ in
                let currentInset = scrollView.contentInset
                let newContentInset = UIEdgeInsets(top: currentInset.top,
                                                   left: currentInset.left,
                                                   bottom: 0,
                                                   right: currentInset.right)
                scrollView.contentInset = newContentInset
                scrollView.scrollIndicatorInsets = newContentInset
            }
    }

    func setupViewModel() {
        viewModel.onStateChange = { [weak self] state in
            guard let strongSelf = self else { return }
            switch state {
            case .initial: break
            case .loading:
                strongSelf.activityIndicator.startAnimating()
                strongSelf.tableView.isHidden = true
            case .loaded:
                strongSelf.activityIndicator.stopAnimating()
                strongSelf.tableView.isHidden = false
                strongSelf.tableViewDataSource = strongSelf.viewModel.currencyAmountViewModels
                strongSelf.tableView.reloadData()
                strongSelf.hideOfflineModeWarning()
            case .updating: break
            case .updated:
                strongSelf.updateCurrencyAmountValues()
                strongSelf.hideOfflineModeWarning()
            case .offline:
                strongSelf.showOfflineModeWarning()
            case .error:
                strongSelf.tableView.isHidden = true
                strongSelf.activityIndicator.stopAnimating()
                strongSelf.showError(message: "Unable to get currency rates") {
                    guard let strongSelf = self else { return }
                    strongSelf.viewModel.start()
                }
            }
        }

        viewModel.onBaseAmountChange = { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.updateCurrencyAmountValues()
        }

        viewModel.start()
    }

    func beginEditingBaseCurrencyAmount() {
        let firstIndexPath = IndexPath(row: 0, section: 0)

        UIView.animate(withDuration: 0.3, animations: {
            self.tableView.scrollToRow(at: firstIndexPath, at: .top, animated: false)
        }, completion: { (finished) in
            guard finished else { return }
            let firstIndexPath = IndexPath(row: 0, section: 0)
            if let cell = self.tableView.cellForRow(at: firstIndexPath) as? CurrencyValueCell {
                cell.amountTextField.isUserInteractionEnabled = true
                cell.amountTextField.becomeFirstResponder()
            }
        })
    }

    func updateCurrencyAmountValues() {
        tableViewDataSource = viewModel.currencyAmountViewModels
        tableView.indexPathsForVisibleRows?.forEach {
            guard let cell = tableView.cellForRow(at: $0) as? CurrencyValueCell,
            cell.amountTextField.isFirstResponder == false else {
                return
            }
            let model = tableViewDataSource[$0.row]
            cell.amountTextField.text = numberFormatter.string(from: NSNumber(value: model.amount))
        }
    }

    func showOfflineModeWarning() {
        let item = UIBarButtonItem(title: "Offline mode",
                                   style: .done,
                                   target: self, action: #selector(showOfflineModeMessage))
        self.navigationItem.rightBarButtonItem = item

    }

    @objc func showOfflineModeMessage() {
        showMessage("Connection appears to be offline. Currency rates may not be actual")
    }

    func hideOfflineModeWarning() {
        self.navigationItem.rightBarButtonItem = nil
    }
}

private extension CurrenciesViewController {

    func showMessage(_ message: String) {
        let controller = UIAlertController(title: "Warning", message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
        controller.addAction(action)
        present(controller, animated: true, completion: nil)
    }

    func showError(message: String, retryAction: @escaping () -> Void) {
        let controller = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "Try again", style: .default) { _ in
            retryAction()
        }
        controller.addAction(action)
        present(controller, animated: true, completion: nil)
    }
}

extension CurrenciesViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        selectBaseCurrency(tableViewDataSource[indexPath.row])
        beginEditingBaseCurrencyAmount()
    }

    func selectBaseCurrency(_ model: CurrenciesViewModel.CurrencyAmountViewModel) {
        viewModel.changeBaseCurrency(to: model.title, withAmount: model.amount)
        let newModels = viewModel.currencyAmountViewModels

        let changeset = StagedChangeset(source: tableViewDataSource, target: newModels)

        tableView.reload(using: changeset, with: .automatic) {
            self.tableViewDataSource = $0
        }
    }
}

extension CurrenciesViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableViewDataSource.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeCellClass(CurrencyValueCell.self, for: indexPath) else {
            return UITableViewCell()
        }

        let model = tableViewDataSource[indexPath.item]
        let currencyTitle = model.title
        let currencyName = model.currencyName
        let value = numberFormatter.string(from: NSNumber(value: model.amount))

        cell.currencyTitleLabel.text = currencyTitle
        cell.currencyNameLabel.text = currencyName
        cell.flagImageView.image = model.image
        cell.amountTextField.text = value
        cell.amountTextField.delegate = self

        return cell
    }
}

extension CurrenciesViewController: UITextFieldDelegate {

    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.isUserInteractionEnabled = false
    }

    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        let oldText = textField.text as NSString? ?? ""
        let resultText = oldText.replacingCharacters(in: range, with: string)
        let fixedText = sanitizeDecimalSeparator(resultText)
            .replacingOccurrences(of: numberFormatter.groupingSeparator, with: "")

        if fixedText.isEmpty {
            viewModel.changeBaseAmount(1)
            return true
        }

        if let amount = numberFormatter.number(from: fixedText)?.doubleValue {
            viewModel.changeBaseAmount(amount)
            return true
        }

        return false
    }

    // https://forums.developer.apple.com/thread/93338
    func sanitizeDecimalSeparator(_ input: String) -> String {

        var charset = CharacterSet.decimalDigits
        charset.insert(charactersIn: numberFormatter.groupingSeparator)

        guard let separator = input.first(where: {
            $0.unicodeScalars.contains(where: {
                charset.contains($0) == false
            })
        }) else {
            return input
        }

        let separatorString = String(separator)
        if  separatorString != numberFormatter.decimalSeparator {
            return input.replacingOccurrences(of: separatorString,
                                              with: numberFormatter.decimalSeparator)
        }
        return input
    }

}
