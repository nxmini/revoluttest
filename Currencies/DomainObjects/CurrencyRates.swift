//
//  CurrencyRate.swift
//  Currencies
//
//  Created by Павел В. Баранов on 10/02/2019.
//

import Foundation

struct CurrencyRates: Codable {
    let base: String
    let rates: [String: Double]
}
