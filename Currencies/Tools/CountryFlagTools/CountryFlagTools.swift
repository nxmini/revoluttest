//
//  CountryFlagTools.swift
//  Currencies
//
//  Created by Павел В. Баранов on 18/02/2019.
//

import Foundation
import FlagKit

extension FlagKit {

    static let knownCountries = ["AUD": "AU", "EUR": "EU", "USD": "US"]

    static func flagImage(currencyCode: String, style: FlagStyle) -> UIImage? {
        let countries = countriesForCurrencyCode(currencyCode)
        for country in countries {
            if let image = Flag(countryCode: country)?.image(style: style) {
                return image
            }
        }
        return nil
    }

    private static func countriesForCurrencyCode(_ currencyCode: String) -> [String] {
        if let knownCountry = knownCountries[currencyCode] { return [knownCountry] }
        let infoObjects = IsoCountryCodes.searchByCurrency(currency: currencyCode)
        return infoObjects.map { $0.alpha2 }
    }
}
