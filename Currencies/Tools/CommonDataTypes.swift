//
//  CommonDataTypes.swift
//  Currencies
//
//  Created by Павел В. Баранов on 09/02/2019.
//

import Foundation
import Result

typealias AppResult<T> = Result<T, AnyError>
typealias AppCompletion<T> = (AppResult<T>) -> Void
