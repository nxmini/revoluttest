//
//  TableView+Cells.swift
//  Currencies
//
//  Created by Павел В. Баранов on 10/02/2019.
//

import UIKit

extension UITableView {

    private func makeReuseIdFor<T>(_ : T.Type) -> String {
        return String(describing: T.self)
    }

    func registerCell<T: UITableViewCell>(_ type: T.Type) {
        let reuseId = makeReuseIdFor(type)
        register(type.self, forCellReuseIdentifier: reuseId)
    }

    func dequeCellClass<T>(_ type: T.Type, for indexPath: IndexPath) -> T? {
        let reuseId = makeReuseIdFor(type)
        return dequeueReusableCell(withIdentifier: reuseId, for: indexPath) as? T
    }
}
