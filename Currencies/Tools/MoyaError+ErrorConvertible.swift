//
//  MoyaError+ErrorConvertible.swift
//  Currencies
//
//  Created by Павел В. Баранов on 09/02/2019.
//

import Foundation
import Moya
import Result

//This makes `.tryMap{...}` magic work
extension MoyaError: ErrorConvertible {

    enum ErrorConvertibleFailure: Error {
        case wrongErrorDuringConvertation
    }

    public static func error(from error: Error) -> MoyaError {
        guard let converted = error as? MoyaError else {
            return .underlying(ErrorConvertibleFailure.wrongErrorDuringConvertation, nil)
        }
        return converted
    }
}
