//
//  TimerActionTrigger.swift
//  Currencies
//
//  Created by Павел В. Баранов on 17/02/2019.
//

import Foundation

class TimerActionTrigger: RepeatingActionTrigger {

    private var action: (() -> Void)?
    private var timer: Timer?
    private let timeInterval: TimeInterval

    init(timeInterval: TimeInterval = 1) {
        self.timeInterval = timeInterval
    }

    func setAction(_ action: @escaping () -> Void) {
        self.action = action
    }

    func start() {
        stop()
        timer = Timer.scheduledTimer(withTimeInterval: timeInterval, repeats: true) { [weak self] _ in
            self?.action?()
        }
    }

    func stop() {
        timer?.invalidate()
        timer = nil
    }

    deinit {
        stop()
    }

}
