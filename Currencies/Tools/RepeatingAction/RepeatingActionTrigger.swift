//
//  RepeatingActionTrigger.swift
//  Currencies
//
//  Created by Павел В. Баранов on 17/02/2019.
//

import Foundation

protocol RepeatingActionTrigger {
    func setAction(_ action: @escaping () -> Void)
    func start()
    func stop()
}
