//
//  RevolutAPI.swift
//  Currencies
//
//  Created by Павел В. Баранов on 09/02/2019.
//

import Foundation
import Moya

enum RevolutAPI {
    case rates(base: String)
}

extension RevolutAPI: TargetType {
    var baseURL: URL {
        return URL(string: Constants.baseUrl)!
    }

    var path: String {
        switch self {
        case .rates: return Constants.Paths.currencyRates
        }
    }

    var method: Moya.Method {
        switch self {
        case .rates: return .get
        }
    }

    var sampleData: Data {
        switch self {
        case .rates: return Data()
        }
    }

    var task: Task {
        switch self {
        case .rates(let base):
            let parameters = [Constants.Parameters.baseCurrency: base]
            return .requestParameters(parameters: parameters,
                                      encoding: URLEncoding.queryString)
        }
    }

    var headers: [String: String]? {
        return ["Content-type": "application/json"]
    }
}

//swiftlint:disable nesting
extension RevolutAPI {
    enum Constants {
        static let baseUrl = "https://revolut.duckdns.org"

        enum Paths {
            static let currencyRates = "latest"
        }

        enum Parameters {
            static let baseCurrency = "base"
        }
    }
}
//swiftlint:enable nesting
